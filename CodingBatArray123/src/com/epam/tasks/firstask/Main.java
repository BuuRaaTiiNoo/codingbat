package com.epam.tasks.firstask;

import java.util.Arrays;
import java.util.Random;

public class Main {

    //Given an array of ints, return the number of 9's in the array.
    private int arrayCount9(int[] nums) {
        int count = 0;
        for (int num : nums){
            if (num == 9){
                count++;
            }
        }
        return count;
    }

    public static void main(String[] args){
        int[] arrNumber = new int[10];
        for (int i = 0; i < arrNumber.length; i++)
            arrNumber[i] = new Random().nextInt(5) + 5;
        System.out.println(Arrays.toString(arrNumber));
        System.out.println(new Main().arrayCount9(arrNumber));
    }
}
