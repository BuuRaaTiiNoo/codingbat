package com.epam.tasks.firstask;

import java.util.Arrays;
import java.util.Random;

public class Main {

    //Given an array of ints, return true if the sequence of numbers 1, 2, 3 appears in the array somewhere.
    private boolean array123(int[] nums){
        StringBuilder s = new StringBuilder();
        for (int num : nums) {
            s.append(num);
        }
        return s.toString().contains("123");
    }

    public static void main(String[] args){
        int[] arrNumber = new int[10];
        for (int i = 0; i < arrNumber.length; i++)
            arrNumber[i] = new Random().nextInt(3) + 1;
        System.out.println(Arrays.toString(arrNumber));
        System.out.println(new Main().array123(arrNumber));
    }
}
