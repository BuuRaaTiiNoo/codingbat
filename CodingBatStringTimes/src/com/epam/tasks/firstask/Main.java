package com.epam.tasks.firstask;

public class Main {

    //Given a string and a non-negative int n, return a larger string that is n copies of the original string.
    private String stringTimes(String str, int n){
        StringBuilder s = new StringBuilder();
        for(int i = 0; i < n; i++){
            s.append(str);
        }
        return s.toString();
    }

    public static void main(String[] args){
        System.out.println(new Main().stringTimes("Asd", 3));
    }
}
