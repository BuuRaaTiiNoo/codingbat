package com.epam.tasks.firstask;

public class Main {

    //Suppose the string "yak" is unlucky. Given a string, return a version where all the "yak" are removed,
    // but the "a" can be any char. The "yak" strings will not overlap.

    private String stringYak(String str) {
        return str.replace("yak","");
    }

    public static void main(String[] args){
        String[] arr = {"yakpak","pakyak","yak123ya","yak","yakxxxyak","HiyakHi"};
        for(String s : arr){
            System.out.print(s + "      ");
            System.out.println(new Main().stringYak(s));
        }
    }
}
