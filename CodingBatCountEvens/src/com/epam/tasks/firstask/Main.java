package com.epam.tasks.firstask;

import java.util.Arrays;
import java.util.Random;

public class Main {

    //Return the number of even ints in the given array. Note: the % "mod" operator computes the remainder, e.g. 5 % 2 is 1.
    private int countEvens(int[] nums) {
        int count = 0;
        for (int num : nums) {
            if (num % 2 == 0) {
                count++;
            }
        }
        return count;
    }

    public static void main(String[] args){
        int[] arrNumber = new int[10];
        for (int i = 0; i < arrNumber.length; i++)
            arrNumber[i] = new Random().nextInt(10);
        System.out.println(Arrays.toString(arrNumber));
        System.out.println(new Main().countEvens(arrNumber));
    }
}
