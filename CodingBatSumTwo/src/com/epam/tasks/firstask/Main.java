package com.epam.tasks.firstask;

import java.util.Arrays;
import java.util.Random;

public class Main {

    //Given an array of ints, return true if the sum of all the 2's in the array is exactly 8.
    private boolean sum28(int[] nums) {
        int sum = 0;
        for (int num : nums) {
            if (num == 2) {
                sum += num;
            }
        }
        return sum == 8;
    }
    public static void main(String[] args){
        int[] arrNumber = new int[10];
        for (int i = 0; i < arrNumber.length; i++)
            arrNumber[i] = new Random().nextInt(3) + 1;
        System.out.println(Arrays.toString(arrNumber));
        System.out.println(new Main().sum28(arrNumber));
    }
}
