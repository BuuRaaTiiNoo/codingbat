package com.epam.tasks.firstask;

import java.util.Arrays;
import java.util.Random;

public class Main {

    //Given a string, return true if the first instance of "x" in the string is immediately followed by another "x".
    private boolean doubleX(String str){
        int positionX = str.indexOf("x");
        return positionX < str.length() - 1 && str.charAt(positionX + 1) == "x".charAt(0);
    }

    public static void main(String[] args){
        String s = "";
        char[] arrNumber = new char[10];
        for (int i = 0; i < arrNumber.length; i++) {
            arrNumber[i] = (char) (new Random().nextInt(6) + 115);
            s += arrNumber[i];
        }
        System.out.println(Arrays.toString(arrNumber));
        System.out.println(new Main().doubleX(s));
    }
}
